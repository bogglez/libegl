/* KallistiOS ##version##

   egl.c
   (c)2017 Stefan Galowicz
*/
            

#include "EGL/egl.h"
#include "GL/gl.h"


#define EGL_ERROR(n) { egl_error = (n); return EGL_FALSE; }

static EGLBoolean is_initialized  = EGL_FALSE;
static EGLint     egl_error       = EGL_SUCCESS;
static EGLenum    current_api     = EGL_OPENGL_ES_API;
static int        created_context = 0;

static struct fb_config {
	unsigned short width;
	unsigned short height;
	unsigned char r_size;
	unsigned char g_size;
	unsigned char b_size;
	unsigned char a_size;
	unsigned char amask_size;
	unsigned char depth_size;
	unsigned char client_version; // for OpenGL ES
	EGLenum       swap_behavior;
} fb_config = { .width = 640, .height = 480, .client_version = 1, .swap_behavior = EGL_BUFFER_PRESERVED};


static int parse_u8(unsigned char * const sz, EGLint const val) {
	if(val >= 0) {
		*sz = val <= 255 ? val : 255;

		return 0;
	}

	return 1;
}

static int is_valid_display(EGLDisplay dpy) {
	return dpy == (EGLDisplay)1;
}

static int is_valid_context(EGLContext ctx) {
	return ctx == (EGLContext)1;
}

static int is_valid_surface(EGLSurface surface) {
	return surface == (EGLSurface)1;
}

// FIXME: most values are not tested
// FIXME: need to parse remaining attributes from https://www.khronos.org/registry/EGL/sdk/docs/man/html/eglChooseConfig.xhtml
static int parse_gl_attrib(EGLint const attrib, EGLint const value) {
	switch(attrib) {
	case EGL_ALPHA_MASK_SIZE:
		return parse_u8(&fb_config.amask_size, value);

	case EGL_ALPHA_SIZE:
		return parse_u8(&fb_config.a_size, value);

	case EGL_BIND_TO_TEXTURE_RGB:
	case EGL_BIND_TO_TEXTURE_RGBA:
		break;

	case EGL_BLUE_SIZE:
		return parse_u8(&fb_config.b_size, value);

	case EGL_BUFFER_SIZE:
	case EGL_COLOR_BUFFER_TYPE:
	case EGL_CONFIG_CAVEAT:
	case EGL_CONFIG_ID:
	case EGL_CONFORMANT:
		break;

	case EGL_DEPTH_SIZE:
		return parse_u8(&fb_config.depth_size, value);

	case EGL_GREEN_SIZE:
		return parse_u8(&fb_config.g_size, value);

	case EGL_LEVEL:
	case EGL_LUMINANCE_SIZE:
	case EGL_MATCH_NATIVE_PIXMAP:
	case EGL_NATIVE_RENDERABLE:
	case EGL_MAX_SWAP_INTERVAL:
	case EGL_MIN_SWAP_INTERVAL:
		break;

	case EGL_RED_SIZE:
		return parse_u8(&fb_config.r_size, value);

	case EGL_SAMPLE_BUFFERS:
	case EGL_SAMPLES:
	case EGL_STENCIL_SIZE:
	case EGL_RENDERABLE_TYPE:
		return value != EGL_OPENGL_BIT && value != EGL_OPENGL_ES_BIT; // no support for GLES2 shaders and OpenVG

	case EGL_SURFACE_TYPE:
		return !!(value & ~(EGL_SWAP_BEHAVIOR_PRESERVED_BIT | EGL_WINDOW_BIT));
		break;

	case EGL_TRANSPARENT_TYPE:
	case EGL_TRANSPARENT_RED_VALUE:
	case EGL_TRANSPARENT_GREEN_VALUE:
	case EGL_TRANSPARENT_BLUE_VALUE:
		break;
	}

	return 1;
}

static int parse_egl_attrib(EGLint const attrib, EGLint const value) {
	if(current_api == EGL_OPENGL_ES_API) {
		switch(attrib) {
		case EGL_CONTEXT_CLIENT_VERSION:
			if(value == 1) {
				fb_config.client_version = 1;
			}
			return value != 1;
		}
	}
	else if(current_api == EGL_OPENGL_API) {
		switch(attrib) {
		case EGL_CONTEXT_CLIENT_VERSION:
			if(value == 1) {
				fb_config.client_version = 1;
			}
			return value != 1;
		}
	}

	return 1;
}

// FIXME: test fb config more thoroughly
static int is_valid_config(EGLConfig const config) {
	struct fb_config const * const cfg = config;

	if(!cfg) {
		return 1; // use default config
	}

	return cfg && cfg->r_size <= 1 && cfg->g_size <= 1 && cfg->b_size <= 1 && cfg->a_size <= 1
		&& !cfg->amask_size // FIXME: can this be implemented?
		&& !cfg->depth_size && cfg->client_version == 1
		&& (cfg->swap_behavior == EGL_BUFFER_PRESERVED || cfg->swap_behavior == EGL_BUFFER_DESTROYED);
}


EGLBoolean EGLAPIENTRY eglChooseConfig(EGLDisplay dpy, const EGLint *attrib_list, EGLConfig *configs, EGLint config_size, EGLint *num_config) {
	if(!is_valid_display(dpy)) {
		EGL_ERROR(EGL_BAD_DISPLAY);
	}

	if(!num_config) {
		EGL_ERROR(EGL_BAD_PARAMETER);
	}

	if(!is_initialized) {
		EGL_ERROR(EGL_NOT_INITIALIZED);
	}


	if(attrib_list) {
		for(EGLint const * attrib = attrib_list; attrib[0] != EGL_NONE && attrib[1] != EGL_NONE; attrib += 2) {
			if(parse_gl_attrib(attrib[0], attrib[1])) {
				EGL_ERROR(EGL_BAD_ATTRIBUTE);
			}
		}
	}

	if(configs && config_size >= 1) {
		configs[0] = &fb_config;
	}

	*num_config = 1;

	return EGL_TRUE;
}


// FIXME: share_context is not implemented
EGLContext EGLAPIENTRY eglCreateContext(EGLDisplay dpy, EGLConfig config, EGLContext share_context, const EGLint *attrib_list) {
	if(current_api == EGL_NONE) {
		EGL_ERROR(EGL_BAD_MATCH);
	}

	if(!is_valid_display(dpy)) {
		EGL_ERROR(EGL_BAD_DISPLAY);
	}

	if(!is_initialized) {
		EGL_ERROR(EGL_NOT_INITIALIZED);
	}

	if(created_context) { // only allow one context
		return EGL_NO_CONTEXT;
	}

	if(attrib_list) {
		for(EGLint const * attrib = attrib_list; attrib[0] != EGL_NONE && attrib[1] != EGL_NONE; attrib += 2) {
			if(parse_egl_attrib(attrib[0], attrib[1])) {
				EGL_ERROR(EGL_BAD_ATTRIBUTE);
			}
		}
	}

	// EGL_BAD_ALLOC

	created_context = 1;

	return (void*)1;
}

EGLSurface EGLAPIENTRY eglCreateWindowSurface(EGLDisplay dpy, EGLConfig config, EGLNativeWindowType win, const EGLint *attrib_list) {
	if(!is_valid_display(dpy)) {
		EGL_ERROR(EGL_BAD_DISPLAY);
	}

	if(!is_initialized) {
		EGL_ERROR(EGL_NOT_INITIALIZED);
	}

	if(!is_valid_config(config)) {
		EGL_ERROR(EGL_BAD_CONFIG);
	}

	if(win) {
		EGL_ERROR(EGL_BAD_NATIVE_WINDOW);
	}

	return (EGLSurface)1;
}

EGLDisplay EGLAPIENTRY eglGetDisplay(EGLNativeDisplayType display_id) {
	if(display_id != EGL_DEFAULT_DISPLAY) {
		return EGL_NO_DISPLAY;
	}

	return (EGLDisplay)1; // Dreamcast only has one display
}

EGLint eglGetError() {
	EGLint e = egl_error;
	egl_error = EGL_SUCCESS;
	return e;
}

EGLBoolean EGLAPIENTRY eglInitialize(EGLDisplay dpy, EGLint *major, EGLint *minor) {
	if(!is_valid_display(dpy)) {
		EGL_ERROR(EGL_BAD_DISPLAY);
	}


	glKosInit();
#if 0
	if(!init_code_here) {
		EGL_ERROR(EGL_NOT_INITIALIZED);
	}
#endif

	if(major) *major = 1;
	if(minor) *minor = 4;

	is_initialized = EGL_TRUE;

	return EGL_TRUE;
}

EGLBoolean EGLAPIENTRY eglMakeCurrent(EGLDisplay dpy, EGLSurface draw, EGLSurface read, EGLContext ctx) {
	if(!is_valid_display(dpy)) {
		EGL_ERROR(EGL_BAD_DISPLAY);
	}

	if(!is_initialized) {
		EGL_ERROR(EGL_NOT_INITIALIZED);
	}

	if(!is_valid_surface(draw) || !is_valid_surface(read)) {
		EGL_ERROR(EGL_BAD_SURFACE);
	}

	if(!is_valid_context(ctx)) {
		EGL_ERROR(EGL_BAD_CONTEXT);
	}

	// TODO BAD_MATCH if draw or read not compatible with context

	if(ctx == EGL_NO_CONTEXT && (draw != EGL_NO_SURFACE || read != EGL_NO_SURFACE)) {
		EGL_ERROR(EGL_BAD_MATCH);
	}

	if(ctx != EGL_NO_CONTEXT && (draw == EGL_NO_SURFACE || read == EGL_NO_SURFACE)) {
		EGL_ERROR(EGL_BAD_MATCH);
	}

	// TODO EGL_BAD_ACCESS if current on other thread

	// TODO BAD_NATIVE_PIXMAP

	// BAD_NATIVE_WINDOW can not happen
	// BAD_CURRENT_SURFACE can not happen

	// TODO BAD_ALLOC

	// EGL_CONTEXT LOST can not happen


	// TODO bind context to this thread

	return EGL_TRUE;
}

EGLBoolean EGLAPIENTRY eglSwapBuffers(EGLDisplay dpy, EGLSurface surface) {
	if(fb_config.swap_behavior == EGL_BUFFER_DESTROYED) {
		glClear(GL_COLOR_BUFFER_BIT);
	}

	glSwapBuffersKOS();

	return GL_TRUE;
}

const char *EGLAPIENTRY eglQueryString (EGLDisplay dpy, EGLint name) {
	if(!is_valid_display(dpy)) {
		EGL_ERROR(EGL_BAD_DISPLAY);
	}

	if(!is_initialized) {
		EGL_ERROR(EGL_NOT_INITIALIZED);
	}

	if(name != EGL_CLIENT_APIS && name != EGL_VENDOR && name != EGL_VERSION && name != EGL_EXTENSIONS) {
		EGL_ERROR(EGL_BAD_PARAMETER);
	}


	switch(name) {
	case EGL_CLIENT_APIS: return "OpenGL OpenGL_ES";
	case EGL_EXTENSIONS:  return "EGL_KHR_surfaceless_opengl";
	case EGL_VENDOR:      return "bogglez";
	case EGL_VERSION:     return "1.4";
	}

	return 0;
}

EGLBoolean EGLAPIENTRY eglBindAPI (EGLenum api) {
	if(api != EGL_OPENGL_API && api != EGL_OPENGL_ES_API) {
		return EGL_FALSE;
	}

	current_api = api;

	return EGL_TRUE;
}

EGLBoolean EGLAPIENTRY eglGetConfigAttrib(EGLDisplay dpy, EGLConfig config, EGLint attribute, EGLint *value) {
	if(!is_valid_display(dpy)) {
		EGL_ERROR(EGL_BAD_DISPLAY);
	}

	if(!is_initialized) {
		EGL_ERROR(EGL_NOT_INITIALIZED);
	}

	if(!is_valid_config(config)) {
		EGL_ERROR(EGL_BAD_CONFIG);
	}

	// TODO check BAD_ATTRIBUTE
	// TODO implement querying

	return EGL_FALSE;
}

EGLBoolean EGLAPIENTRY eglTerminate(EGLDisplay dpy) {
	if(!is_valid_display(dpy)) {
		EGL_ERROR(EGL_BAD_DISPLAY);
	}


	// TODO: releases resources associated with an EGL display connection except if they're made current
	// This will only happen if somebody uses EGL on two threads.

	return EGL_TRUE;
}

EGLBoolean EGLAPIENTRY eglDestroyContext(EGLDisplay dpy, EGLContext ctx) {
	if(!is_valid_display(dpy)) {
		EGL_ERROR(EGL_BAD_DISPLAY);
	}

	if(!is_initialized) {
		EGL_ERROR(EGL_NOT_INITIALIZED);
	}

	if(!is_valid_context(ctx)) {
		EGL_ERROR(EGL_BAD_CONTEXT);
	}


	// no context to destroy in this implementation

	return EGL_TRUE;
}

EGLBoolean EGLAPIENTRY eglDestroySurface(EGLDisplay dpy, EGLSurface surface) {
	if(!is_valid_display(dpy)) {
		EGL_ERROR(EGL_BAD_DISPLAY);
	}

	if(!is_initialized) {
		EGL_ERROR(EGL_NOT_INITIALIZED);
	}

	if(!is_valid_surface(surface)) {
		EGL_ERROR(EGL_BAD_SURFACE);
	}


	// no surface to destroy in this implementation

	return EGL_TRUE;
}

EGLBoolean EGLAPIENTRY eglQueryContext(EGLDisplay dpy, EGLContext ctx, EGLint attribute, EGLint *value) {
	if(!is_valid_display(dpy)) {
		EGL_ERROR(EGL_BAD_DISPLAY);
	}

	if(!is_initialized) {
		EGL_ERROR(EGL_NOT_INITIALIZED);
	}

	if(!is_valid_context(ctx)) {
		EGL_ERROR(EGL_BAD_CONTEXT);
	}

	if(!value) {
		EGL_ERROR(EGL_BAD_ATTRIBUTE); // not specified in docs
	}


	switch(attribute) {
	case EGL_CONFIG_ID: *value = 1; break;
	case EGL_CONTEXT_CLIENT_TYPE: *value = current_api; break;
	case EGL_CONTEXT_CLIENT_VERSION: *value = 1; break; // for GL ES. "not meaningful" for OpenGL but no error
	case EGL_RENDER_BUFFER: *value = EGL_BACK_BUFFER; break;
	default: EGL_ERROR(EGL_BAD_ATTRIBUTE);
	}

	return EGL_TRUE;
}

EGLBoolean EGLAPIENTRY eglQuerySurface(EGLDisplay dpy, EGLSurface surface, EGLint attribute, EGLint *value) {
	if(!is_valid_display(dpy)) {
		EGL_ERROR(EGL_BAD_DISPLAY);
	}

	if(!is_initialized) {
		EGL_ERROR(EGL_NOT_INITIALIZED);
	}

	if(!is_valid_surface(surface)) {
		EGL_ERROR(EGL_BAD_SURFACE);
	}

	if(!value) {
		EGL_ERROR(EGL_BAD_ATTRIBUTE); // not specified in docs
	}


	switch(attribute) {
	case EGL_CONFIG_ID: *value = 1; break;
	case EGL_HEIGHT: *value = fb_config.height; break;
	case EGL_HORIZONTAL_RESOLUTION: *value = EGL_UNKNOWN; break;
	case EGL_LARGEST_PBUFFER: break; // do nothing for windows
	case EGL_MIPMAP_LEVEL: break; // do nothing for windows
	case EGL_MIPMAP_TEXTURE: break; // do nothing for windows
	case EGL_MULTISAMPLE_RESOLVE: *value = EGL_MULTISAMPLE_RESOLVE_DEFAULT; break;
	case EGL_PIXEL_ASPECT_RATIO: *value = EGL_UNKNOWN; break;
	case EGL_RENDER_BUFFER: *value = EGL_BACK_BUFFER; break;
	case EGL_SWAP_BEHAVIOR: *value = fb_config.swap_behavior; break;
	case EGL_TEXTURE_FORMAT: break; // do nothing for windows
	case EGL_TEXTURE_TARGET: break; // do nothing for windows
	case EGL_VERTICAL_RESOLUTION: *value = EGL_UNKNOWN; break;
	case EGL_WIDTH: *value = fb_config.width; break;
	default: EGL_ERROR(EGL_BAD_ATTRIBUTE);
	}

	return EGL_TRUE;
}

__eglMustCastToProperFunctionPointerType EGLAPIENTRY eglGetProcAddress(const char *procname) {
	return 0;
}
