INSTALL=install
INSTALL_DATA=${INSTALL} -m 644
INSTALL_PROGRAM=$(INSTALL)

prefix=$(KOS_PORTS)
exec_prefix=$(prefix)
includedir=$(prefix)/include
libdir=$(exec_prefix)/lib

CC       = $(KOS_CC)
ARFLAGS  = rcs
CPPFLAGS = $(filter -D% -I%,$(KOS_CFLAGS))
CFLAGS   = $(filter-out -D% -I%,$(KOS_CFLAGS)) -I$(srcdir)include -std=gnu11 -Ofast
LDFLAGS  = $(KOS_LDFLAGS) $(KOS_LIB_PATHS)
LDLIBS   = $(KOS_LIBS)


libEGL.a: $(patsubst %.c,libEGL.a(%.o),$(wildcard $(srcdir)*.c))

clean:
	$(RM) $(wildcard *.o)

distclean:
	$(RM) $(wildcard *.o *.a)

installdirs:
	mkdir -p $(DESTDIR)$(libdir) $(DESTDIR)$(includedir)/EGL $(DESTDIR)$(includedir)/KHR

install: libEGL.a installdirs
	$(INSTALL_DATA) $(addprefix $(srcdir)include/EGL/,egl.h eglext.h eglplatform.h) $(DESTDIR)$(includedir)/EGL
	$(INSTALL_DATA) $(srcdir)include/KHR/khrplatform.h $(DESTDIR)$(includedir)/KHR
	$(INSTALL_DATA) libEGL.a $(DESTDIR)$(libdir)

uninstall:
	$(RM) $(addprefix $(DESTDIR)$(includedir)/EGL/,egl.h eglext.h)
	$(RM) $(DESTDIR)$(includedir)/KHR/khrplatform.h
	-rmdir $(DESTDIR)$(includedir)/GL
	-rmdir $(DESTDIR)$(includedir)/KHR
